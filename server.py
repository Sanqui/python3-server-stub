#!/usr/bin/python3
from sys import argv
import logging
import asyncio
#logging.basicConfig(level=logging.DEBUG)

BINDHOST = '127.0.0.1'
BINDPORT = 12345


class ServerProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        print('Connection from {}'.format(peername))
        self.transport = transport
        
    def data_received(self, data):
        message = data

        #self.transport.close()
    
    def _parse(self, line):
        pass
        
    def _send(self, command):
        self.transport.write(packet.encode('utf-8'))
    

loop = asyncio.get_event_loop()
#loop.set_debug(True)
server_coro = loop.create_server(ServerProtocol, BINDHOST, BINDPORT)
server = loop.run_until_complete(server_coro)

# Serve requests until Ctrl+C is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

# Close the server
server.close()
loop.run_until_complete(server.wait_closed())
loop.close()

